const { buildAuthenticatedRouter } = require('admin-bro-expressjs');
const argon2 = require('argon2');
const Users = require('../models/models.users');

const buildAdminRouter = (admin) => {
	const router = buildAuthenticatedRouter(
		admin,
		{
			cookieName: 'admin-bro',
			cookiePassword: 'some_password',
			authenticate: async (email, password) => {
				const user = await Users.findOne({ email });
				if (user && (await argon2.verify(user.encryptedPassword, password)) && user.admin) {
					return user.toJSON();
				}
				return null;
			}
		},
		null,
		{
			resave: false,
			saveUninitialized: true
		}
	);
	return router;
};

module.exports = buildAdminRouter;
