const AdminBro = require('admin-bro');
const AdminBroMongoose = require('admin-bro-mongoose');
const Messages = require('../models/MessagesModel');
const SubscribeBuyer = require('../models/SubscribeBuyerModel');
const SubscribeGallery = require('../models/SubscribeGalleryModel');
// const UsersAdmin = require('./resource_options/users.admin');

AdminBro.registerAdapter(AdminBroMongoose);

const options = {
	resources: [ Messages, SubscribeBuyer, SubscribeGallery ] 
};

module.exports = options;
