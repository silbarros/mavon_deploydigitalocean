const Messages = require('../models/MessagesModel');
const validator = require('validator');
const validatePhoneNumber = require('validate-phone-number-node-js');


class MessagesController {
  // GET FIND ALL
  async findAll(req, res){
    try{
        const messages = await Messages.find({});
        res.send(messages);
    }
    catch(e){
        res.send({e})
    }
  }

  // Add message to DB
  async addMessage(req, res){
    const {name:contactName, phone:contactPhone,email:contactEmail, message:contactMessage}=req.body
    if (!contactName || !contactEmail || !contactMessage) return res.json({ ok: false, messageOutput: 'All fields are required' });
    if (!validator.isEmail(contactEmail)) return res.json({ ok: false, messageOutput: 'Please provide a valid email' });
    if(contactPhone){
      if (!validatePhoneNumber.validate(contactPhone)) return res.json({ ok: false, messageOutput: 'Please provide a valid phone number' });
    }
    try{
        Messages.create({name:contactName,phone:contactPhone,email:contactEmail,message:contactMessage})
        res.json({ ok: true, messageOutput: `Message successfully sent`});
      }
    catch(e){
      res.json({ ok: false, error });
    }
  }

  


      
  async deleteMessage(req,res){
    let {id:messagID}=req.body
    let exists= await Messages.exists({_id:messagID})
    try{
      if(exists){
        await Messages.deleteOne({_id:messagID});
        res.send(`The message with ID ${messagID} was successfully deleted`)
      }else{
        res.send(`The message with ID ${messagID} does not exist in the DB and hence cannot be deleted`)
      }
    }
    catch(error){
      res.send({error})
    }
  }

};

module.exports = new MessagesController();