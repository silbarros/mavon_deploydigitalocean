const Buyers = require('../models/SubscribeBuyerModel');
const validator = require('validator');

class SubscribeBuyerController {
  // GET FIND ALL
  async findAll(req, res){
    try{
        const buyers = await Buyers.find({});
        res.send(buyers);
    }
    catch(e){
        res.send({e})
    }
  }

  // Add message to DB
  async addBuyer(req, res){
    const {firstName:contactName, lastName:contactLastName, email:contactEmail}=req.body
    if (!contactName || !contactEmail) return res.json({ ok: false, messageOutput: 'First name and email are required' });
    if (!validator.isEmail(contactEmail)) return res.json({ ok: false, messageOutput: 'Please provide a valid email' });
    try{
      const user = await Buyers.findOne({ email:contactEmail });
      if (user) return res.json({ ok: false, messageOutput: 'The provided email is already subscribed' });
        Buyers.create({firstName:contactName,lastName:contactLastName, email:contactEmail})
        res.json({ ok: true, messageOutput: 'Successfully subscribed' });
      }
    catch(e){
      res.json({ ok: false, error });
    }
  }

  


      
  async deleteBuyer(req,res){
    let {id:messagID}=req.body
    let exists= await Messages.exists({_id:messagID})
    try{
      if(exists){
        await Messages.deleteOne({_id:messagID});
        res.send(`The message with ID ${messagID} was successfully deleted`)
      }else{
        res.send(`The message with ID ${messagID} does not exist in the DB and hence cannot be deleted`)
      }
    }
    catch(error){
      res.send({error})
    }
  }

};

module.exports = new SubscribeBuyerController();