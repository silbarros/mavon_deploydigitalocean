const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const subscribeGallerySchema = new Schema({
    name: { type: String, required: true },
    email: { type: String, unique: false, required: true },
})
module.exports =  mongoose.model('subscribegallery', subscribeGallerySchema);