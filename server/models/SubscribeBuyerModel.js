const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const subscribeBuyerSchema = new Schema({
    firstName: { type: String, required: true },
    lastName: { type: String, required: false },
    email: { type: String, unique: false, required: true },
})
module.exports =  mongoose.model('subscribebuyer', subscribeBuyerSchema);