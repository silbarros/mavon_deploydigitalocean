const express = require('express'),
    app = express(),
    mongoose = require('mongoose'),
    messagesRoute = require('./routes/MessagesRoute'),
    subscribeBuyerRoute = require('./routes/SubscribeBuyerRoute'),
    subscribeGalleryRoute = require('./routes/SubscribeGalleryRoute'),
    bodyParser = require('body-parser');
    require('dotenv').config();
// ===================== Admin Bro ===================
const AdminBro = require('admin-bro');
const options = require('./admin/admin.options');
const buildAdminRouter = require('./admin/admin.router');
const admin = new AdminBro(options);
const router = buildAdminRouter(admin);
app.use(admin.options.rootPath, router);

// =================== initial settings ===================
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// connnect to mongo
const cors = require('cors');
var port = process.env.PORT || 3040 // for digital ocean deployment
// const corsOptions ={ // for local deployment
//     origin:'http://localhost:3000', 
//     credentials:true,            //access-control-allow-credentials:true
//     optionSuccessStatus:200
// }
app.use(cors(corsOptions));

// connecting to mongo and checking if DB is running
async function connecting(){
try {
    await mongoose.connect(/*'mongodb+srv://silviabarros:15084287@ecommerceapp.jns2e.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'*/'mongodb+srv://anne:20070613al@cluster0.c5leq.mongodb.net/mavondb?retryWrites=true&w=majority', { useUnifiedTopology: true , useNewUrlParser: true })
    console.log('Connected to the DB')
} catch ( error ) {
    console.log('ERROR: Seems like your DB is not running, please start it up !!!');
}
}
connecting()
// temp stuff to suppress internal warning of mongoose which would be updated by them soon
mongoose.set('useCreateIndex', true);
// end of connecting to mongo and checking if DB is running

// routes
app.use('/messages', messagesRoute);
app.use('/subscribe-buyer', subscribeBuyerRoute);
app.use('/subscribe-gallery', subscribeGalleryRoute);
app.use('/users', require('./routes/routes.users'))

// Set the server to listen on port 3001
app.listen(port, () => console.log(`listening on port ${port}`))
