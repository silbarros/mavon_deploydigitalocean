// const URL = `http://localhost:3001`;
import axios from 'axios'
// =======  preparing to the deplyment  ========
const URL = window.location.hostname === `localhost`
            // ? `http://localhost:3001` // 3001 is the server port for local deployment
            ? `http://localhost:3040` // 3040 is the server port for digital ocean deployment
            : `http://159.89.12.221` // it should be replaced with actual domain during the deployment
// =============================================
const customInstance = axios.create ({
  baseURL : URL, 
  headers: {'Accept': 'application/json'}
})

export default customInstance;

export { URL };
