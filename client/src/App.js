import React, { useState } from "react";
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import './App.css';
import Header from "./pages/Header";
import Home from './pages/Home'
import About from './pages/About'
import Mission from './pages/Mission'
import Subscribe from './pages/Subscribe';
import Footer from './pages/Footer';
import Contact from './pages/Contact';
import Gallery from './pages/Gallery'
import ScrollToTop from './components/scrollToTop';
import ArtPiece from './pages/ArtPiece';

// import { withAuthenticator, AmplifySignOut } from '@aws-amplify/ui-react'



function App() {
  return (
    <div className="App">
      <Router>
      <ScrollToTop />
      {/* <Route component={AmplifySignOut} /> */}

        {/* <Route component={Header}/> */}
        <Route render={props => <Header {...props} />}/>


        <Route exact path='/contact-us' component={Contact}/>

        <Route exact path='/' component={Home}/>

        <Route exact path='/' component={About}/>

        <Route exact path='/' component={Mission}/>

        <Route exact path='/' component={Subscribe}/>

        <Route exact path='/gallery' component={Gallery} />

        <Route exact path='/gallery/artpiece' component={ArtPiece} />

        <Footer />
      </Router>

    </div>
  );
}

export default App;
// export default withAuthenticator(App);

