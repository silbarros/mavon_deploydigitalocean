import React, { useState } from 'react'
import {NavLink} from 'react-router-dom'
import Carousel from './Carousel'


const Gallery = () => {

  return <div>
    <div className='section grid-two-columns'>
      <div /*style={{ marginLeft: 109 }}*/>
        <p style={{ fontSize: 12, fontFamily: 'Poppins', fontWeight: 400 }}>Home page&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;＞&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gallery&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;＞&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gallery 101</p>

        <h1 style={{ fontSize: 45, fontFamily: 'Playfair Display', fontWeight: 400, color: '#22282B', marginTop: 38.5, marginBottom: 0 }}>Gallery 101, Berlin</h1>

        <p style={{ fontSize: 18, fontFamily: 'Avenir', fontWeight: 400, color: '#404040', maxWidth: 520, marginTop: 47, marginBottom: 0 }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

        <button className='get-started-button' style={{ marginTop: 26, }}>See Gallery</button>
      </div>

      <div>
        <img src='https://picsum.photos/526/687' style={{ borderRadius: 8 }} />
      </div>
    </div>


    <hr style={{ backgroundColor: "#cccccc", border: "none", height: 1 }} />


    <div className='section section-artists'>
      <h1 style={{ fontSize: 45, fontFamily: 'Playfair Display', fontWeight: 400, color: '#22282B', paddingTop: 71, marginTop: 0, marginBottom: 0, marginLeft: 109 }}>Artists</h1>

      <Carousel show={4}>
        <div>
          <div className='carousel-artists-img'>
            <img src='https://picsum.photos/338/338' />
            <p>artist name</p>
          </div>
        </div>
        <div>
          <div className='carousel-artists-img'>
            <img src='https://picsum.photos/338/338' />
            <p>artist name</p>
          </div>
        </div>
        <div>
          <div className='carousel-artists-img'>
            <img src='https://picsum.photos/338/338' />
            <p>artist name</p>
          </div>
        </div>
        <div>
          <div className='carousel-artists-img'>
            <img src='https://picsum.photos/338/338' />
            <p>artist name</p>
          </div>
        </div>
        <div>
          <div className='carousel-artists-img'>
            <img src='https://picsum.photos/338/338' />
            <p>artist name</p>   
          </div>
        </div>
        </Carousel> 
      </div>

    <hr style={{ backgroundColor: "#cccccc", border: "none", height: 1 }} />


    <div className='section section-artpiece'>
      <h1 style={{ fontSize: 45, fontFamily: 'Playfair Display', fontWeight: 400, color: '#22282B', paddingTop: 71, marginTop: 0, marginBottom: 0, marginLeft: 109 }}>Art Piece</h1>

      <Carousel show={3}>
        <div>
          <div className='carousel-artpiece-img'>
            <NavLink exact to={'/gallery/artpiece'}><img src='https://picsum.photos/511/511'/>  </NavLink>
            {/* <img src='https://picsum.photos/511/511'/>   */}
          </div>
        </div>
        <div>
          <div className='carousel-artpiece-img'>
          <NavLink exact to={'/gallery/artpiece'}><img src='https://picsum.photos/511/511'/>  </NavLink>
            {/* <img src='https://picsum.photos/511/511'/>   */}          </div>
        </div>
        <div>
          <div className='carousel-artpiece-img'>
          <NavLink exact to={'/gallery/artpiece'}><img src='https://picsum.photos/511/511'/>  </NavLink>
            {/* <img src='https://picsum.photos/511/511'/>   */}          </div>
        </div>
        <div>
          <div className='carousel-artpiece-img'>
          <NavLink exact to={'/gallery/artpiece'}><img src='https://picsum.photos/511/511'/>  </NavLink>
            {/* <img src='https://picsum.photos/511/511'/>   */}          </div>
        </div>
        <div>
          <div className='carousel-artpiece-img'>
          <NavLink exact to={'/gallery/artpiece'}><img src='https://picsum.photos/511/511'/>  </NavLink>
            {/* <img src='https://picsum.photos/511/511'/>   */}          </div>
        </div>
      </Carousel> 

    </div>

  </div>
}

export default Gallery