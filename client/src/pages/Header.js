import React, {useState} from 'react'
import {NavLink} from 'react-router-dom'
import { Link } from 'react-router-dom'
import transparentLogo from '../images/transparentLogo.png'
import '../App.css';
import * as Scroll from 'react-scroll'

const Header = (props) => {

  const [menuIsOpened, setMenuIsOpened] = useState(false)

  const Link = Scroll.Link
  var currentPage = window.location.pathname;
  console.log('href===>',window.location.pathname) // returns the absolute URL of a page
  return (
    <div className='header'>
    <nav role="navigation">
      <div id="menuToggle">
        <p onClick={()=>setMenuIsOpened(true)}>二</p>

        {menuIsOpened
        ?<div >

        <ul id="menu" className='four-columns-grid'>

          <div className='menu-left'>
            <li>
              <span onClick={()=>(
                props.history.push('/'),
                setMenuIsOpened(false)
              )}><h1 className='top-bar'>Home</h1></span>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
            </li>

            <li>
              <h1 className='bottom-bar'>About Us</h1>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
            </li>
          </div>

          <div className='menu-left'>
            <li>
              <h1 className='top-bar'>Our Mission</h1>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
            </li>

            <li>
              <span onClick={()=>(
                props.history.push('/contact-us'),
                setMenuIsOpened(false)
              )}><h1 className='bottom-bar'>Contact Page</h1></span>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
            </li>
          </div>

          <div className='menu-left'>
            <li>
              <span onClick={()=>(
                props.history.push('/gallery'),
                setMenuIsOpened(false)
                )}><h1 className='top-bar'>Galleries</h1></span>
              <NavLink exact to={'/gallery'}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</NavLink>
              {/* <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p> */}
            </li>

            <li>
              <h1 className='bottom-bar'>My Profile</h1>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
            </li>
          </div>

          <div className='vertical-line'></div>

          <div className='menu-right'>
            <h2 onClick={()=>setMenuIsOpened(false)} className='close-button'>✕</h2>
            <img src={transparentLogo} alt='mavon logo'></img>
            <div className='p-wrapper'><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p></div>
            <div className='menu-link'>
              <a href="#" target="_blank"><i className="fab fa-twitter"></i></a>
              <a href="#" target="_blank"><i className="fab fa-discord"></i></a>
              <a href="#" target="_blank"><i className="fab fa-facebook"></i></a>
              <a href="#" target="_blank"><i className="fab fa-instagram"></i></a>
            </div>
          </div>
          </ul>
          </div>
          : null
          }

        </div>
      </nav>

    {currentPage=='/'?
      <Link activeClass="active"
        to='home'
        smooth={true}
        hashSpy={true}
        duration={500}
        isDynamic={true}
        ignoreCancelEvents={false}
        spyThrottle={500}
      >
        <img src={transparentLogo} alt='mavon logo' className='logo'></img>
      </Link>:
      <NavLink exact to={'/'}>
        <img src={transparentLogo} alt='mavon logo' className='logo'></img>
      </NavLink>
    }

      <NavLink exact to={'/contact-us'} className='contact-us-button'>contact&nbsp;us</NavLink>

    </div>
  )
}

export default Header