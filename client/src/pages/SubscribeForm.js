import React, { useState } from "react";
import axios from 'axios';
import { URL } from '../config';
import '../App.css';

function App() {
  const [whoIsSubscribing, setWhoIsSubscribing] = useState('buyer')
  const [ formBuyer, setValues ] = useState({
		name: '',
		lastName: '',
		email: '',
	});
  const [ formGallery, setValuesGal ] = useState({
		name: '',
		email: '',
	});
  const [message,setMessage] = useState('')

  const handleChangeBuyer = (evt) => {
		setValues({ ...formBuyer, [evt.target.name]: evt.target.value });
	};
  
  const handleChangeGallery = (evt) => {
		setValuesGal({ ...formGallery, [evt.target.name]: evt.target.value });
	};

  const handleSubmitBuyer = async(evt) => {
		evt.preventDefault();
		try {
			const response = await axios.post(`${URL}/subscribe-buyer/add`, {
				firstName: formBuyer.name,
				lastName: formBuyer.lastName,
				email: formBuyer.email,
			});
			setMessage(response.data.messageOutput);
      setValues({[evt.target.value]:''})
      setTimeout(() => {
        setMessage('')
      }, 1000);
      evt.target.reset()
		} catch (error) {
			console.log(error);
		}
	};

  const handleSubmitGallery = async(evt) => {
		evt.preventDefault();
		try {
			const response = await axios.post(`${URL}/subscribe-gallery/add`, {
				name: formGallery.name,
				email: formGallery.email,
			});
			setMessage(response.data.messageOutput);
      setValuesGal({[evt.target.value]:''})
      setTimeout(() => {
        setMessage('')
      }, 1000);
      evt.target.reset()
		} catch (error) {
			console.log(error);
		}
	};

  return (
    <div >
      <h2 className='i-am-a'>I am a...</h2>
      <div>
        <input
          type='radio'
          onChange={e => setWhoIsSubscribing('buyer')}
          name='whoIsSubscribing'
          defaultChecked
        />
        <label>Buyer</label>

        <input
          type='radio'
          onChange={e => setWhoIsSubscribing('gallery')}
          name='whoIsSubscribing'
        />
        <label>Gallery</label>
      </div>

      {whoIsSubscribing === 'buyer'
        ? <form className='subscribe-form' onChange={handleChangeBuyer} onSubmit={handleSubmitBuyer}>
          <div className='subscribe-name-wrapper'>
            <input name='name' placeholder='First name' />
            <input name='lastName' placeholder='Last name' />
            <input className='subscribe-email' name='email' placeholder='Your email' />
            <button className='subscribe-button'>Subscribe</button>
          </div>
          <div className='subscribe-message'>{message}</div>
        </form>
        : whoIsSubscribing === 'gallery'
          && <form className='subscribe-form' onChange={handleChangeGallery} onSubmit={handleSubmitGallery}>
            <div className='subscribe-gallery-name-wrapper'>
              <input className='subscribe-gallery-name' name='name' placeholder='Gallery name' />
              <input className='subscribe-email' name='email' placeholder='Your email' />
              <button className='subscribe-button'>Subscribe</button>
            </div>
            <div className='subscribe-message'>{message}</div>
          </form>}

      
    </div>
  );
}

export default App;
