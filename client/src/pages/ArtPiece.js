import React from 'react';
import Carousel from './Carousel';


const ArtPiece = () => {

  return <div>
    <div className='section'  style={{backgroundColor: 'pink'}} style={{display: 'grid', gridTemplateColumns: '1fr 1fr'}}>
      <div style={{marginLeft: 109}}>
        <p style={{ fontSize: 12, fontFamily: 'Poppins', fontWeight: 400, marginTop: 39.5 }}>Home page&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;＞&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gallery&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;＞&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gallery 101&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;＞&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Art piece name</p>
        <h1 style={{ fontSize: 45, fontFamily: 'Playfair Display', fontWeight: 400, color: '#22282B', marginTop: 38.5, marginBottom: 0 }}>Art Piece Name</h1>
        <p style={{ fontSize: 18, fontFamily: 'Avenir', fontWeight: 400, color: '#404040', maxWidth: 520, marginTop: 40, marginBottom: 0 }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

        <h1 style={{ fontSize: 45, fontFamily: 'Playfair Display', fontWeight: 400, color: '#22282B', marginTop: 20 , marginBottom: 0 }}>Artist Name</h1>
        <p style={{ fontSize: 18, fontFamily: 'Avenir', fontWeight: 400, color: '#404040', maxWidth: 520, marginTop: 40, marginBottom: 0 }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim </p>
        <button  className='get-started-button' style={{marginTop: 26}}>Contact</button>
      </div>

      <div style={{margin: 'auto'}}>
        <img src='https://picsum.photos/511/511' />
      </div>
    </div>

    <hr style={{ backgroundColor: "#cccccc", border: "none", height: 1 }} />

    <div className='section section-artpiece'>
      <h1 style={{ fontSize: 45, fontFamily: 'Playfair Display', fontWeight: 400, color: '#22282B', paddingTop: 71, marginTop: 0, marginBottom: 0, marginLeft: 109 }}>See More...</h1>

      <Carousel show={3}>
        <div>
          <div className='carousel-artpiece-img'>
            <img src='https://picsum.photos/511/511'/>  
          </div>
        </div>
        <div>
          <div className='carousel-artpiece-img'>
            <img src='https://picsum.photos/511/511'/>  
          </div>
        </div>
        <div>
          <div className='carousel-artpiece-img'>
            <img src='https://picsum.photos/511/511'/>  
          </div>
        </div>
        <div>
          <div className='carousel-artpiece-img'>
            <img src='https://picsum.photos/511/511'/>  
          </div>
        </div>
        <div>
          <div className='carousel-artpiece-img'>
            <img src='https://picsum.photos/511/511'/>  
          </div>
        </div>
      </Carousel> 



      {/* <div style={{ overflowX: 'auto', overflowY: 'hidden', whiteSpace: 'nowrap', paddingTop: 58 }}>
        <img src='https://picsum.photos/511/511' style={{ marginRight: 60 }} />
        <img src='https://picsum.photos/511/511' style={{ marginRight: 60 }} />
        <img src='https://picsum.photos/511/511' style={{ marginRight: 60 }} />
        <img src='https://picsum.photos/511/511' style={{ marginRight: 60 }} />
        <img src='https://picsum.photos/511/511' style={{ marginRight: 60 }} />
      </div> */}
    </div>
  </div>
}

export default ArtPiece